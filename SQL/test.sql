INSERT INTO seasons VALUES ('0', 'Summer'), ('1', 'Autumn'), ('2', 'Winter'), ('3', 'Spring');


CREATE TABLE plants (plant_key int primary key, plant_name text,);


ALTER TABLE conditions
  add CONSTRAINT uniqueSeasonPlants UNIQUE (plant, season);

  SELECT *
    FROM USER_CONSTRAINTS
   WHERE table_name = 'conditions';

create table polygon(polygon_key int primary key);

create schema plant_schema;

  select * from information_schema.schemata;

ALTER TABLE conditions
SET schema plant_schema;

CREATE TABLE plant_schema.polygon (polygon_key serial);

SELECT AddGeometryColumn ('plant_schema','polygon','polygon_desc',4326,'POLYGON',2);

SELECT f_geometry_column As col_name, type, srid, coord_dimension As ndims
    FROM geometry_columns
    WHERE f_table_name = 'polygon' AND f_table_schema = 'plant_schema';

    
