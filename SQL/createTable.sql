CREATE TABLE plant (
  plantKey SERIAL PRIMARY KEY,
  name varChar(50) NOT NULL,
  scientific varChar(50) NOT NULL,
  planting varChar(500),
  growing varChar(500),
  pests varChar(500),
  harvesting varChar(500),
  info varChar(500)
);


CREATE TABLE seasons (
  seasonKey int PRIMARY KEY,
  seasonDesc varChar(30) NOT NULL
);

CREATE TABLE zone (
  zoneID SERIAL PRIMARY KEY,
  zoneDesc varChar(30)
);

--Add geometry column
SELECT AddGeometryColumn ('zone','geom',4326,'POLYGON',2, false);

--conditions table
CREATE TABLE conditions (
  conditionKey SERIAL PRIMARY KEY,
  plantKey int references plant(plantKey),
  seasonKey int references seasons(seasonKey),
  zoneID int references zone(zoneID)
);

INSERT INTO seasons VALUES
(0, 'Summer');

-- Create Index
CREATE INDEX zone_index ON zone USING GIST(geom);

-- Alter data types
ALTER TABLE plant
ALTER COLUMN name TYPE varChar(50),
ALTER COLUMN scientific TYPE varChar(50),
ALTER COLUMN planting TYPE varChar(500),
ALTER COLUMN growing TYPE varChar(500),
ALTER COLUMN pests TYPE varChar(500),
ALTER COLUMN harvesting TYPE varChar(500);

UPDATE zone
SET zonedesc = 'Arid' where zonedesc = 'Arid';
