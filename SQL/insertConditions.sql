/**
  Insert the conditions into the plant DB

  -----Structure-----
    conditions
      - conditionKey - SERIAL
      - plantKey - int - plant(plantKey)
      - seasonKey - int - season(seasonKey)
      - zoneID - int - zone(zoneID)
  -------------------

  **/

-- -- Basil
-- INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
--   (3,0,4),(3,0,7),(3,1,4),(3,3,4),(3,3,7);
-- -- Coriander
-- INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
--   (2,0,4),(2,0,7),(2,1,4),(2,1,5),(2,1,6),(2,1,7),(2,2,7),
--   (2,3,4),(2,3,7);

-- Asparagus
INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
  (4,2,6),(4,2,7);
-- Beans
INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
  (5,0,4),(5,1,7);
-- Broccoli
INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
  (6,0,7),(6,1,4),(6,1,5),(6,1,6),(6,2,4),(6,2,5),(6,2,6),(6,2,7),(6,3,4),(6,4,7));
-- Capsicum and Chillies
INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
  (7,0,4),(7,0,5),(7,0,7),(7,1,4),(7,2,4),(7,3,4),(7,3,5),(7,3,6),(7,3,7);
-- Carrots
INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
  (8,0,7),(8,1,4),(8,1,7),(8,2,4),(8,3,4),(8,3,7);
-- Citrus trees
INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
  (9,1,6),(9,1,7);
-- Eggplant
INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
  (10,0,4),(10,0,7),(10,1,4),(10,2,4),(10,3,4),(10,3,7);
-- Garlic
INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
  (11,1,4),(11,1,5),(11,1,6),(11,1,7),(11,2,4),(11,2,7);
-- Kiwi Fruit
INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
  (12,1,7),(12,2,7);
-- Lettuce
INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
  (13,0,4),(13,0,5),(13,0,6),(13,0,7),(13,1,4),(13,1,5),(13,1,6),(13,1,7),
  (13,2,4),(13,2,5),(13,2,6),(13,2,7),(13,3,4),(13,3,5),(13,3,6),(13,3,7);
-- Onions
INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
  (14,0,4),(14,0,5),(14,0,6),(14,0,7),(14,1,4),(14,1,5),(14,1,6),(14,1,7),(14,2,5),(14,2,6),
  (14,2,7),(14,3,4),(14,3,5),(14,3,6),(14,3,7);
-- Parsley
INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
  (15,0,4),(15,0,6),(15,0,7),(15,1,4),(15,1,5),(15,1,6),(15,1,7),(15,2,5),
  (15,2,7),(15,3,4),(15,3,7);
-- Peas
INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
  (16,1,4),(16,1,6),(16,1,7),(16,2,4),(16,2,7);
-- Pumpkins
INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
  (17,0,4),(17,0,7),(17,1,4),(17,2,4),(17,2,5),(17,2,6),(17,3,4),(17,3,7);
-- Spinach
INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
  (18,1,4),(18,1,5),(18,1,6),(18,2,4),(18,2,5),(18,2,6),(18,2,7),(18,3,4),(18,3,7);
-- Strawberry
INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
  (19,3,7);
-- Sweet Corn
INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
  (20,0,4),(20,0,6),(20,0,7),(20,1,4),(20,2,4),(20,3,4),(20,3,5),(20,3,6),(20,3,7);
-- Thyme
INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
  (21,0,7),(21,1,4),(21,1,5),(21,1,6),(21,1,7),(21,2,7),(21,3,6),(21,3,7);
-- Tomato
INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
  (22,0,4),(22,0,6),(22,0,7),(22,1,5),(22,1,6),(22,2,4),(22,3,4),(22,3,5),
  (22,3,6),(22,3,7);
-- Watermelon
INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
  (23,3,4),(23,3,5);
-- Zucchini
INSERT INTO conditions (plantKey, seasonKey, zoneID) VALUES
  (24,0,7),(24,1,4),(24,3,4),(24,3,7);