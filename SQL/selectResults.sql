  SELECT p.name, p.scientific, p.planting, p.growing, p.pests, p.harvesting
  FROM conditions as c, plant AS p, zone AS z, seasons AS s
  WHERE c.plantKey = p.plantKey
    AND c.zoneID = z.zoneID
    AND c.seasonKey = s.seasonKey
    AND s.seasonKey = season
    AND ST_Contains(zone.polygon, ST_MakePoint(logitude, latitude));

-- Point in zone
SELECT zoneid, zonedesc FROM zone
WHERE st_contains(geom, ST_GeomFromText('POINT(131.176368 -12.647055)', 4326));

-- Testing
SELECT p.name
FROM conditions as c, plant AS p, zone AS z, seasons AS s
WHERE c.plantKey = p.plantKey
  AND c.zoneID = z.zoneID
  AND c.seasonKey = s.seasonKey
  AND s.seasonDesc = 'Autumn'
  AND z.zoneDesc = 'Humid';
