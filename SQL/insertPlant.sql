/**
 Inserting into plant database

 Write one for each unique entry

 Plant Entry
  - ID (auto increment)
  - Plant name
  - Scientific name
  - Planting info
  - Growing info
  - Pest info
  - Harvesting info

Example
INSERT INTO plant (name, scientific, planting, growing, pests, harvesting)
VALUES(strawberry, xxx, plantstuff, growstuff, peststuff, harveststuff);

**/

-- Plant Name (change this for each entry)
-- INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
-- VALUES();

--Asparagus
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES('Asparagus', 'Asparagus officinalis', 'A crisp, delicious addition to any salad this is a flowering perennial ' ||
 'plant species. These are filled with nutrients and are a great addition to a vegetable patch and in sandwiches at ' ||
  'your next high tea.', 'Planted as either crowns or seedlings, this is a family plant that needs its own ' ||
   'little bed as they will continue to breed for 15-20 years.Plant three or four crowns for a family about 40 ' ||
    'cm apart in their bed. Best planted in a raised area that has had compost and manure worked through in a 1:3 ' ||
     'ratio. Plant on a small ridge at the bottom of a trench about 20 cm deep and 30 cm wide placing the roots at ' ||
      'either side. Cover the roots with 5 cm of soil leaving crown at the surface.', 'Top with cow manure each ' ||
       'winter, water well in summer and keep weed free.', 'New crops susceptible to weeds, thrips and cutworms are ' ||
        'the major insect pests.', 'Do not harvest in the first year while the plant establishes a root system and ' ||
         'prune back to the ground in winter after the fern has been established. in the second year harvest no more ' ||
          'than half the spears and leave any less than 1 cm in diameter. Expect to be able to harvest about 80% of ' ||
           'the spears over a 10 week period every 2-3 days in peak growing season at the beginning of spring.');

-- -- Basil
-- INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
-- VALUES ('Basil', 'Ocimum basilicum', 'A fine leaved, strong smelling plant that adds lots of flavour to any dish. ' ||
--  'Commonly used in Mediterranean and south east Asian cooking, like Thai and Malaysian.', 'If planting from seeds, ' ||
--   'start the seeds in trays, plant out in 4-6 weeks. Sow the seeds on the surface. When planted outside plant 20-25 ' ||
--    'cm apart. Plant on a coo day. Grows well next to tomato, which provides some shade. Can also be grown inside in ' ||
--     'pots.',
--    'Basil likes being fed, and liquid seaweed, worm wee or liquid manure should be applied fairly frequently through ' ||
--     'the growing season. Particularly important if you are picking the plant often. Prone to frost, so when watering ' ||
--      'do not water the foliage. Ensure that the soil remains moist.', 'Watch out for slugs and caterpillars - common ' ||
--       'pests to the basil plant.', 'Basil is best harvested picking leaves as needed. Ensure to pick flowering stems' ||
--        ' to lengthen the life of the plant.');
--Beans
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES('Beans', 'Phaseolus vulgaris','An easy plant to grow, this climber is an excellent choice when space is an ' ||
'issue and just as excellent for your health.', 'These guys love full sun in general, except for in super-hot, dry and windy ' ||
  'conditions. They need support when they reach around 2 m high. The seeds should be planted in rich, deep, pH ' ||
   'neutral and organic rich soil. Use a fertiliser at planting time and mulch your bed. No need to soak the seeds, ' ||
    'just plant in damp - not wet - soil', 'Be careful not to over-water, especially when the planting and the ' ||
     'seedlings are young. Otherwise guide the plants on the structure you choose.', 'Careful not to over-water or ' ||
      'over-fertilise. Keep an eye out for halo blight fungus, will appear as leaf spots with holes which progress to' ||
       ' light green leaves with dark green veins. remove and destroy infected plants - do not put into compost or ' ||
        'use as organic mulch.','Beans will be ready in 12-14 weeks. Pick the bean pods when young and tender ' ||
         'before the pods look lumpy. Harvest in regularly to promote more flowering.');
--Beetroot
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES('Beetroot', 'Betta vulgaris', 'A simple delicious tuber that tastes so much better than the one from the can. ' ||
 'These plants are cannot be beeten with their ease of growing and the deliciousness.', 'These need a rich, ' ||
   'well-drained soil filled with organic matter. Drainage is key. Soak the seeds overnight before planting in a 2 cm' ||
    'deep trench. Plant 2 cm apart and cover with fluffy soil. keep the top damp and around the two week mark thin  ' ||
     'them out leaving 6-8 cm between plants.', 'Feeding is the key to a sweet and tender beetroot, as with feeding it '||
      'will grow faster. Manure based fertiliser in combination with seaweed works well. Water regularly and deeply', ||
       'Generally pest-free in a good patch. Too much water too early will usually kill them off.', 'Remove when crown'||
        'is seen above the surface, around 10 weeks. Harvest before they get too big else they can be tough and taste average.');
--Broccoli
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES('Broccoli', 'Brassica oleracea', 'Very similar to its brother cauliflower, these evolved cabbages have a '||
 'delicious flowering head packed full of goodness.', 'This is a cool season vegetable, and prefers a sunny place '||
  'sheltered from strong winds. Leave about 40 cm between seedlings when planting in a well-drained soil filled with '||
    'organic matter with a pH around 6.5-7.', 'Weekly feeding and compost tea especially leading up to harvest is good. '||
      'Some worm wee or fish fertiliser will improve size and shape. Water deeply to prevent stress, water needs to '||
        'reach the roots.', 'The cabbage white butterfly caterpiller is the bane of the broccoli plant. It also suffers'||
          'aphids, caterpillars and water stress.', 'This will take anywhere from 12 weeks onwards. Look for '||
            'tightly formed heads and cut with a knife. Ensure not to let it flower.');

--Cauliflwer
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES('Cauliflower', 'Brassica oleracea', 'Very similar to its sister broccoli, these evolved '||
  'cabbages have a delicious flowering head packed full of goodness that tastes delicious with cheese.', 'This '||
    'is a cool season vegetable, and prefers a sunny place sheltered from strong winds. Leave about 40 cm between '||
      'seedlings when planting in a well-drained soil filled with organic matter with a pH around 6.5-7.', ||
       'Weekly feeding and compost tea especially leading up to harvest is good. Some worm wee or fish fertiliser will '||
        'improve size and shape. Water deeply to prevent stress, water needs to reach the roots.', 'The cabbage white '||
          'butterfly caterpiller is the bane of the cauliflower plant. It also suffers aphids, caterpillars and water stress.',||
            'This will take anywhere from 12 weeks onwards. Look for tightly formed heads and cut with a knife. Ensure '||
              'not to let it flower.');
--Capsicum
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES('Capsicum and Chillies', 'Capsicum annuum', 'This spicy family has a wide range of multicoloured members, from '||
  'the sweet, large capsicum to the smaller spicy chilli species. These always put a bang in your dishes', 'These need '||
    'a warm sunny spot that is not very windy. They do not like the frost and in cold areas they will often die back '||
      'over winter before re-shooting in spring. A shadecloth does not hurt in really, really hot summers. They grow '||
        'well in pots but in the garden need 50-60 cm of personal space to get some air movement and warm soil. Prepare '||
          'the soil a month before hand by mixing in some chicken poo fertiliser.', 'These crispy bursts of flavour do '||
            'not really need any fertiliser. They are deep-rooted so need some regular, deep soakings. If you are unsure '||
              'stick your finger in the soil to check if it is damp.', 'Pretty much pest free as most pests can not  '||
                'handle the heat. Capsicums can be susceptible to blossom end rot and fruit fly. They can also suffer '||
                  'sunburn like a red head on the beach.', 'Chillies can be picked just about whenever you '||
                    'please. You can even wait until they are dried and shriveled on the bush. The more you pick the '||
                      'happier the plant. Capsicum can be picked any time, but do not leave them on the plant too long'||
                        'else thy attract pests. Cut them off the plant instead of pulling.');

--Carrots
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES('Carrots', 'Daucus carota', 'Crunchy sticks off orange gold these are a favourite due to their variety and '||
  'sweetness. They can be stewed, dipped, grated, steamed and eaten right out of the ground - washed of course.', 'Need '||
    'light clear soil or you will get deformed carrots. Some put it in a raised bed of compost and potting mix to help '||
      'drainage. Plant as seeds in a trench 2 cm deep, tapping seeds into the trenchh. fill up the trench and press '||
        'lightly. Keep the soil damp until the pop out.', 'At 5 cm high cull the population so there is about 3 cm between '||
          'plants, then again when they reach 12-15 cm high cull so there is about 5 cm between them. Over watering is '||
            'a serious issue but the key is to ensure the soil remains damp.', 'The only pest is the carrot fly. Use '||
              'plenty of compost and some good companion plants like spring onions.', 'Start harvesting after'||
                'about 8 weeks. Harvest as needed buy ensure they are all removed before 16-18 weeks.');
--Citrus
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES ('Citrus trees', 'Citrus x limon, x paradisi, x sinensis', 'Citrus is a great addition to any garden, adding '||
  'delicious smells of the blossoms, they look pretty and the fruits are a great addition to any dish.', 'Citrus '||
    'trees love sun, at least 5 hours a day for maximum fruiting. The trees need to be planted in well drained soil.'||
      'The trees need at least 4.5 m between them, with 8-9 meters being even better. They can also be grown in pots.',
        'Citrus trees have some shallow roots that need protection that can be achieved by mulching. The best fruit '||
          'achieved with citrus food during fruiting season.', 'Sadly citrus trees are prone to pests such as as the '||
            'citrus leafminer, borers, and the lightbrown apple moth. Sooty mould is another common fungal problem.',
              'The length of time until a tree begins to produce fruit varies, but generally between 3 and 6 '||
                'years old. The length of time varies between the citrus type, climate, warm weather and rainfall. '||
                  'Harvesting is easy. When the fruits are of colour and they fall into your had when you go to pick '||
                    'them, then are are ready!');
--Cucumber
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES('Cucumber', 'Cucumis sativus', 'Used in many dishes, eaten anywhere from fresh to picked. They may not be as '||
  'the pretty cucumbers from the supermarkets they taste delicious in any case.', 'In cool places they need a bit of '||
    'sun, but in hot sun they need some shade to encourage them. They are pretty much vines so just like any growing '||
      'teen they need some support. Plant in a mound about 40 cm across, two cucumbers per mound. A good mulch will '||
        'prevent soil dryness and fungal disease protection.', 'They - much like the caterpillar - are quite hungry '||
          'and will consume anything they are fed. They are also thirsty and drip irrigation works miracles under a '||
            'layer of mulch. Ensure their foliage is not wet if manually watering, but do not let them dry out.', ||
              'These are susceptible to fungal infections, and prevention is better than treatment so keep everything '||
                'clean around them and fresh mulch. Hand pollination may be necessary if there is a low bee population.',||
                  'The variety determines harvesting, so pick according to the advised lengths. Cucumbers generally '||
                    'take 8-10 weeks to ripen. Monitor the vines as under-ripe is better than over-ripe');

-- --Coriander
-- INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
-- VALUES ('Coriander', 'Coriandrum sativum', 'A common addition to Asian or Asian inspired cooking, it is a strongly ' ||
--  'flavoured herb that adds flavour to any dish.', 'This does well in a pot or in the ground. Position the plant where
--   it will receive shade on sunny days if you are in a place that gets scorching summers. seeds should be sown 0.5 -1 ' ||
--    'cm deep. and successful seedlings 20-30 cm apart.', 'Does not need much food if soil is healthy. Some compost or ' ||
--     'liquid seaweed fertiliser does well if needed. Ensure soil is moist, particularly for coriander in pots. If the ' ||
--      'soil is dry the coriander will bolt so monitor the moisture and water as needed.', 'Generally no pests, bolting' ||
--       ' is the biggest issue. In fact is often planted as a companion plant as the smell is unappealing.', 'This can ' ||
--        'be picked at any time but generally wait until foliage is about 20 cm high. Either chop off foliage when you ' ||
--         'want, else pull out the whole plant. The whole plant can be used, as the stems and roots are filled with ' ||
--          'flavour.')

--Eggplants
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES('Eggplant', 'Solanum Melongena', 'A species of nightshade, the only thing deadly about this is the emoji. It '||
  'is used in a variety of cuisines from Asia to the Mediterranean.', 'This plant likes things a bit warmer than its '||
    'close relative the tomato. They do not handle frosts and need a wonderful sunny spot with about 50 cm between '||
      'plants. Do not plant them where tomatoes have been within at least three years, they like a fertile soil that '||
        'drains well. It some mulch that does not touch the stem helps with keeping soil moist.', 'Feed them with ' ||
          'chook poo based fertilisers just as flower buds appear. Staking eggplants helps it to remain standing '||
            'upright when the heavy fruits. A solid stake about 10 cm away loosely attached with a stocking works well.',
              'The plants do not suffer from an enormous number of pests and diseases but fruit fly can be an issues '||
                'in some warmer climates.', 'Takes 10-13 weeks to mature depending on where you live. They '||
                  'are ready to pick as soon as the fruit looks big enough to eat. If they are on the plants too long '||
                    'then they will shrivel up and are attractive to fruit flies.');

--Garlic
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES('Garlic', 'Allium sativum', 'A seasoning that forms the basis for manny dishes around the world, this plant '||
  'makes a delightful addition to any dish. There are two main types - hard and soft neck. The soft neck are like the '||
    'ones bught at the store, and better suit warm temperate regions.', 'Garlic grows well in full sun to part shade, '||
      'is a great companion plant to tomatoes, and improves the scents of roses. Add some organic matter to the soil a '||
        'few weeks before planting and ensure soil is loose and drains. Separate the bulb into cloves and place them '||
          'in the soil pointy side up 3 cm deep in rows 10-15 cm apart.', 'Ensure soil remains moist the first few '||
            'after planting. Seaweed tea and mulch as soon as the shoots have appeared helps. After that they are '||
              'generally alright unless it is particularly dry.', 'Generally safe from pests, they are occasionally '||
                'attacked by aphid which is easily taken care of by a little garlic spray.', 'Best to wait '||
                  'until the flower has started to brown and die. Harvesting the plant earlier and individual cloves '||
                    'not yet have formed. The stems can be used when green like chives when cooked.');

--Kiwi Fruit
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES ('Kiwi Fruit', 'Actinidia deliciosa','As the scientific name suggests, these highly evolved Chinese gooseberries '||
  'are little bursts of deliciousness. As long as a few requirements are met they are quite easy to grow.', 'Kiwi '||
    'fruits need quite a bit of space, 4-6 m apart. As they are vines, training them on a fence works well. '||
      'You need to ensure that you have a male and a female plant, as they do not self produce. They need well drained '||
        'soil in an area that gets plenty. If in a warmer region, ensure there is protection from the scorching sun.'||
          'Ensure the soil drains well.', 'Kiwi fruits have a thick growth. In the first year prune it back to the '||
            'main trunk, leaving two main arms. Each winter prune it vigorously - the fruit only appears on year old '||
              'shoots. In temperatures over 35 degC you must have mulch and water, and you must water the plant a lot '||
                'over the first three years.', 'Frost will kill your fruit in the first few years of growth. Aside '||
                  'from that, the leafrollers, spider mites, thrips, boxelder bugs and nematodes are sometimes problems.',
                    'Fruit should appear after 2-3 years. Some people recommend picking before frost, others '||
                      'think it sweetens them. Pick the fruit and wait from three days to two weeks for them to ripen '||
                        'indoors. This lets them soften inside. Pick the latest kiwi first, they are best stored on '||
                          'the vine.');
--Lettuce
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES('Lettuce', 'Lactuca sativa', 'Lettuce, the basis to so many salads. Its leafy goodness has become commonplace '||
  'in our meals. With the variety of types and therefore flavours, soon you will have all your neighbours lining up '||
    'for some of your lettuce.', 'Lettuce prefers to be planted in cooler weather. If you live in a hot area consider '||
      'planting under taller, leafier plants to shade the lettuce. The plant the seeds in soil about 0.5 cm deep and '||
        ' 15-20 cm apart after thinning, in well drained soil.', 'Lettuce is a heavy feeder as it needs to be grown '||
          'quickly to produce many leaves per plant. It is more sustainable to properly prepare beds else large amounts '||
            'of fertiliser may be necessary. Water is vital as lettuce have shallow roots, and keeping it well mulched '||
              'helps with water retention.', 'Snails are the biggest enemy of the lettuce, with slugs and earwigs coming '||
                'at a close second. There are several methods that help with this, such as spreading coffee grounds '||
                  'around the plants.', 'Lettuces vary in maturity time depending on the variety. This is '||
                    'generally 10-12 weeks, with some ready as early as 6 weeks.');

--Onions
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES('Onions', 'Allum cepa', 'Onion, it makes you cry but with the flavours it adds to meals it is worth it.'||
  'With a wide variety of species, whatever you choose to plant will make a great winter crop.', 'Careful to plant in '||
    'right season else they may bolt. Plant in a sunny, open position with well drained soil. Sow seeds in beds or '||
      'punnets. Transplant at 10 cm high. Plant 10-15 cm apart in rows 20-30 cm apart. Lay on the ground and cover ,' ||
        'roots with soil. If planted directly in a bed, sprinkle and cover lightly with soil. Thin at two weeks.',
          'The onions will not require much fertiliser if put in a well prepared bed with only light watering, being '||
            'ground plants.', 'Onions failing to grow is caused by growing a variety unsuited to the climate. Downy '||
              'mildew occurs in damp conditions, a copper based spray can help. White rot and onion thrip are difficult '||
                'to control.', 'Onions will be ready in 6-8 months once, all the leaves will dry and fall '||
                  'over. leave in the sun for a few days until the skin is dry. Spring onion and shallots do not need '||
                    'thinning and can be harvested from 8-12 weeks.');
--Parsley
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES('Parsley', 'Petroselinum crispum', 'Parsley is forever being used as a garnish, but surprisingly adds a great '||
  'amount flavour to many dishes. Not only that but it is choc full of iron and other healthy nutrients.', 'Flat leaf '||
    'parsley loves a spot in the hot sun, whereas curly parsley is better in a position with only about 4 hours of '||
      'sunlight a day. Plant in rich soil with compost and a neutral pH. It also makes a great companion plant for '||
        'tomatoes and asparagus. It is often planted in a plot.', 'Parsley loves damp soil and will bolt if let to dry. '||
          'Water every second morning is generally a good rotation. It responds well to regular feeding, a liquid '||
            'seaweed feed weekly until 20-25 cm tall does wonders.', 'Parsley suffers from few issues except for '||
              'sometimes snails and slugs. Parsley is susceptible to bolting to seed, so trim any flowers unless you '||
                'it to propagate.', 'Generally wait until two rows of strong stalks have formed. Harvest from '||
                  'the outside. It should continue to grow for years to come.');

--Peas
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES ('Peas', 'Pisum sativum', 'A great winter plant, these climbers will have the neighbours climbing over the '||
  'fence to steal them. There is nothing like peas straight from the vine.', 'Prepare the bed about five weeks before '||
    'planting with old chook poo and compost, maintain a pH of 6.5-7.5. Plant in a place that gets full sun and is '||
      'frost-free with a trellis or similar structure. Plant them no closer than 15 cm to each other.', 'Feeding them '||
        'some blood and bone a couple of times throughout the season and some seaweed based fertiliser every three '||
          'weeks. Only water when soil feels dry, and avoid watering the foliage', 'The main problems to watch out '||
            'for are mildew, the cold, bird attacks and overwatering.', 'Most peas are ready to harvest from '||
              '11-14 weeks. Frequent and continuous harvesting will prolong a crop as the more they are picked the '||
                'more fruit is produced. The number of weeks of harvest will vary between varieties.');

--Potatoes
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES ('Potatoes','Solanum tuberosum','Boil them, mash them, stick them in a stew. These versatile '||
  'vegetables are a staple on any plate so why not your garden too?','Potatoes are fussy. They have personal '||
    'space problems, they are slow to grow and can absorb such things as heavy metals. A great option is to plant '||
      'the seeds in a large pot or tub. Potato plants need a lot of sun, great drainage, and do not plant them where '||
        'chillies, capsicum, tomatoes or eggplants have been planted recently. The best way to plant them is to make '||
          'a mix of compost with aged cow manure and straw. Lay a dept of 15-20cm on top of where you are planting '||
            'them and water it. Lay seed potatoes 25 cm between each, and cover with another layer of straw mix to '||
              'about 15 cm. Continue to pile on the straw mix through the growing season until a depth of about 60 cm.',
        'Potatoes are very hungry and need a lot of food, hence the straw mix. Some seaweed tea at planting and '||
                'flowering helps. Potatoes do not need much watering. When you add each layer of mix a bit of water '||
                  'will do just fine.','Being problem children, potatoes need good planning to avoid them. '||
                    'Ensure good drainage and crop rotation. Potato scab can be avoided by avoiding alkaline soils.',
              'Depending on the variety, potatoes will be ready for harvest anywhere from 12-20 weeks. Chat '||
                        'potatoes are harvested what the lower leaves turn yellow, late season are harvested after '||
                          'their top foliage is dead. Harvest new spuds on a meal to meal basis. Late season spuds are '||
                            'best stored in the soil, but if you are forgetful, they are ready when their skin is '||
                              'hard and does not rub off.');

--Pumpkins
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES ('Pumpkins','Cucurbita pepo','Part of the squash family. They probably will not be growing '||
  'into carriages any time soon, however they make delicious soups and roasts.','These guys love space, and a '||
    'lot of it. A sunny spot, out of the way is what they like best. They love compost, and the vines will root in '||
      'whatever ground they come int contact with. If planting seeds, put in about 2-3 cm, and about 1.5-2 m apart.',
        'They love compost. Give them more. They love water nearly as much, so keep the soil moist. A little '||
          'mulch helps wth the moisture retention.','Pests are not much of a problem but fuzzy mildews may cause '||
            'a problem. The biggest issue is pollination. Hand pollination works well if it is not fruiting properly. ',
              'Pumpkins take from 70-120 days to mature. It is worth it though. To tell that they are ripe '||
                'knock on the side and it will sound hollow. The tendril close to the pumpkin will also be dead.');

--Strawberries
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES ('Strawberry','Fragaria ananassa','Consumed in large quantities world wide, this little berry '||
  'makes a sweet addition to any garden. There is nothing better to have a munch on whilst doing the rest of your '||
    'gardening.','They like some well drained soil, put some compost in the soil before planting. Bought as '||
      'seedlings or stolen runners from friendly neighbours, plant them about 30-100 cm apart. The crown of the roots '||
        'should just be uncovered.','If you want your strawberries sweet and delicious, give them some liquid '||
          'fertiliser when they start to flower. Mulch around the plants ensures soil retention, and suppresses weeds.'||
            'They will also grow runners that will take root, so cut them off and leave the parent growing. They need '||
              'plenty of water but hate water on their leaves so ideally a drip irrigation system.','Insect pests'||
                'such as thrips, two-spotted mite, caterpillars and curl grubs enjoy the plants, as do slugs and snails.',
                  'From planting they will be ready in about 3-4 months. It is easy to tell when they are ready'||
                    'as the berries will be a gorgeous red colour. They will be sweetest if left red on the plant for '||
                      'a day or two.');

--Spinach
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES ('Spinach','Spinacia oleracea','This leafy green wonder grows as strong as it makes the people '||
  'who eat it. They are easy to grow too.','Prepare the garden bed with compost, and sow the seed 1-2 cm deep '||
    'Space the plants 20-30 cm apart. It is compatible with several different plants such as cauliflower, eggplant, '||
      'onion and strawberries. ','A bit of liquid feed every fortnight, and a steady watering assists in '||
        'growth. If harvesting a few leaves at a time, harvest from the outside.','Keep an eye out for downy '||
          'mildew in long damp periods, remove the worst affected leaves. Cucumber mosaic virus is another problem - '||
           'remove and burn the leaves. Pick off caterpillars and destroy.','Either harvest bit at a time '||
            'from the outside. There are edible leaves from about 5 weeks and will produce edible leaves for about 4 '||
              'weeks. If you are harvesting whole plants, you should be able to from 8-10 weeks.');

--Sweet Corn
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES ('Sweet Corn','Zea mays','Why let McCain rain on the tin roof when you could be you eating '||
  'home-grown cobs? Corn seed can also be replanted, making it a sustainable species savior.','Position is key.'||
    'It needs full sun and protection from wind. They grow best in groups encouraging cross pollination. Plant seeds '||
      'about 40-50 cm apart. Climbing beans and cucumbers are great companion plants once the corn has grown up a bit. '||
        'Corn grows best in soil rich with organic matter, and are not a fan of clay. Mulch is one of their best friends',
          'if planted into rich soil it does not need much. Some manure tea after establishment and when '||
            'flowering helps. They also love a bit of water about three times a week.','Corn is not only loved by '||
              'us but also by bushrats, and more-so the "earworm".','Corn is ready when the tassels out the '||
                'tip of the cob are brown and shriveled and the husks are not glossy any longer.');

--Tomato
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES ('Tomatoe','Lycopersicon escultenum','The tomato is the pride of any home gardener. They are '||
  'juicy and delicious and you get the sweet satisfaction of having grown them yourself.','The more sun the '||
    'better for these, unless you are in a super hot spot then a shadecloth helps. Seedlings go in right up to their '||
      'bottom leaves. If growing from seeds, best to start indoors in punnets to germinate. Plant just below the soil '||
        'in potting mix. At 10-15 cm tall they can be planted out with a touch of liquid fertiliser. Put some mulch '||
          'around the plants to assist with moisture retention.','More compost, and some liquid chook poo is '||
            'all the food they need during flowering. They will also need to be staked to help with support during '||
              'fruiting and guide their growth. Never let the soil dry out.','Garden grubs love these guys, so '||
                'rotate your plants, and ensure a good variety of plants to attract the good insects that take care of '||
                  'the pests for you.','Big tomatoes take from 10-14 weeks to mature and cheery tomatoes take '||
                    'from 11-13 weeks. Some pick them more ripe, others slightly firm so as they do not split if left '||
                      'on the plant.');

--Thyme
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES('Thyme', ' Thymus vulgaris', 'An evergreen herb with over 350 varieties, it is a close relative of mint with '||
  'culinary, medicinal and ornamental uses. If you have none it is about thyme you did.', 'Plant in a raised bed with '||
    'a little bit of compost worked through it. Allow 20 cm between each plant as it is low growing and spreads.',
       'Thyme is super low maintenance. Thyme needs no watering, one of the biggest issues being over watering. '||
       'Trimming back the flowers will promote vigorous bushy growths.', 'No real pests to worry about.',
        'Ready to be picked when you want to use some.');

--Watermelon
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES ('Watermelon', 'Citrullus lanatus', 'What better on a warm summer day than a few cold slices of watermelon? '||
  'Even better having grown it by yourself.', 'Warmer climates sow the seeds directly into the soil. In cooler zones '||
    'plant indoor for a month, then carefully transplant. Ensure the soil is filled with good organic matter and '||
      'nutrients as watermelons are heavy feeders. If you are growing multiple plants, space them 2 feet apart in a '||
        '5-foot-wide-hill else 6 foot apart between all plants. Ensure the soil is well drained.', 'Mulch with black '||
          'plastic or straw. Watering is important until they begin fruiting, then only keep the soil moist. When '||
            'the fruits are forming reduce watering, as dry weather produces the sweetest fruit. As the fruit is '||
              'ripening place cardboard or straw under the fruit to prevent rotting.', 'Aphids, cucumber beetles, '||
                'squash vine borer moths and fusariam wilt are all things to watch out for.', 'The harvest '||
                  'time is key, as they do not sweeten after picking. They ripen over two weeks. A ripe watermelon '||
                    'will have a yellow base instead of a white one, and there is little contrast between stripes.');

--Zucchini
INSERT INTO plant (name, scientific, info, planting, growing, pests, harvesting)
VALUES ('Zucchini','Cucurbita pepo','A type of squash zucchinis range in size. An easy plant to grow '||
  'this is great for a beginner gardener.','Plant near some other flowering plants after good soil preparation. '||
    'This will help with pollination. Generally planted first in punnets, the seeds should go down in at about 1 cm.' ||
      'Plant the strong ones in soil 70-100 cm away from each other, and surround plants with mulch.','Ensure '||
        'the soil is kept damp, by good mulching. With good soil preparation, feeding should be unnecessary.',
          'The only real problem that you may experience with zucchinis is lack of pollination. Had pollination '||
            'may therefore be required.','Ready from 6-8 weeks regularly harvest to encourage the plants to '||
              'continuously fruit. The flowers are also edible, great in salads, as a garnish or even fried.');
