<?php
$jsonStuff = '[
  {
    "name": "strawberry",
    "scientific": "strawberrus somthingus",
    "planting": "plant it in places and stuff",
    "growing": "grow the stuff in the dirt and things",
    "pests": "eww icky bugs and stuff",
    "harvesting": "harvest all of the things with the stuff"
  },
  {
    "name": "carrot",
    "scientific": "carrot mccarrotface",
    "planting": "plant it and let it go ooh bby",
    "growing": "grow it and bro it boy",
    "pests": "yucky mole ooooooohh no",
    "harvesting": "pick it up and pull the pick"
  },
  {
    "name": "tomato",
    "scientific": "tommy pickles",
    "planting": "hey this is a plant, so dig it",
    "growing": "grow blow in slow mo bro",
    "pests": "watch out for them bloody kookaburras",
    "harvesting": "pick them off the bush and put them in your mouth"
  }
]';

$resultarray = json_decode($jsonStuff, true);

echo $resultarray;
?>
