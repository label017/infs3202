<?php
//$host = "34.210.108.116";
$host = "localhost";
$user = "postgres";
$pass = "fabregas94";
$db = "postgres";
$port = 5432;
$charset = 'utf8';

// $seas = 1;
// $long = 152.66509374999998;
// $lat = -28.959234386814217;

$lat = $_GET["latitude"];
$long = $_GET["longitude"];
$seas = $_GET["season"];

// $dsn = "pgsql:host=$host;dbname=$db";
// $opt = [
//     PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
//     PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
//     PDO::ATTR_EMULATE_PREPARES   => false,
// ];
$dsn = "pgsql:host=$host;dbname=$db";

//$name = 'Coriander';

try {
    $pdo = new PDO($dsn, $user, $pass);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

// $sql = "SELECT p.plantKey, p.name, p.scientific, p.planting, p.growing, p.pests, p.harvesting, p.info
// FROM conditions as c, plant AS p, zone AS z, seasons AS s
// WHERE c.plantKey = p.plantKey
//   AND c.zoneID = z.zoneID
//   AND c.seasonKey = s.seasonKey
//   AND s.seasonKey = ?
//   AND st_contains(geom, ST_GeomFromText('POINT(? ?)', 4326))";

$sql = "SELECT p.plantKey, p.name, p.scientific, p.planting, p.growing, p.pests, p.harvesting, p.info
FROM conditions as c, plant AS p, zone AS z, seasons AS s
WHERE c.plantKey = p.plantKey
  AND c.zoneID = z.zoneID
  AND c.seasonKey = s.seasonKey
  AND s.seasonKey = ?
  AND ST_Intersects(ST_SetSRID(ST_MakePoint(?, ?),4326), geom)";


//$sql = 'SELECT scientific FROM plant WHERE name = ?';

$stmt = $pdo->prepare($sql);

 $stmt->execute([$seas, $long, $lat]);

// $stmt->execute([$seas, $long, $lat]);

while ($row = $stmt->fetch()) {
      $ID = $row['plantKey'];
      $name = $row['name'];
      $scien = $row['scientific'];
      $planting = $row['planting'];
      $growing = $row['growing'];
      $pests = $row['pests'];
      $harvest = $row['harvesting'];
      $info = $row['info'];


      // Create the search results
      echo "<a href='#modal$name' class='result-link' data-toggle='modal'>";
      echo "<div class='results_item'>";
      echo "<img class='plant-img' src='assets/images/plants/$name.png'>";
      echo "<span class='plant_name'>$name</span></div></a>";

      //Create the modal
      echo "<div class='result-modal modal fade' id='modal$name' role='dialog' aria-hidden='true'>";
      echo "<div class='modal-content'><div class='close-modal' data-dismiss='modal'>";
      echo "<div class='lr'><div class='rl'></div></div></div>";
      echo "<div class='container'><div class='modal-body'>";
      echo "<img src='assets/images/plants/$name.png'><h2>$name</h2><hr>";
      echo "<h4><i>$scien</i></h4><br><h5>General</h5><p>$info</p><h5>Planting</h5><p>$planting</p><h5>Growing</h5><p>$growing</p>";
      echo "<h5>Pests</h5><p>$pests</p><h5>Harvesting</h5><p>$harvest</p>";
      echo "</div></div></div></div>";
}
 ?>