<html>
<head>
  <link href="assets/css/style.css"rel="stylesheet" type="text/css"/>
  <link href="assets/bootstrap/css/bootstrap.min.css"rel="stylesheet" type="text/css"/>
    <!-- jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!-- Bootstrap Core JavaScript -->
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  
</head>

<body>
  <?php
    include ('dbConnect.php');
    $db = new postgresDB();

    $lat = $_GET["latitude"];
    $long = $_GET["longitude"];
    $seas = $_GET["season"];
    $servername = "localhost";
    $username = "postgres";
    $password = "fabregas94";
    $dbname = "postgres";

    $conn_string = "host=$servername dbname=$dbname user=$username password=$password";

    $db->connect($conn_string);


    // $dbconn = pg_connect($conn_string)
    //   or die('Could not connect: ' . pg_last_error());

    //$sql = "SELECT seasonDesc FROM seasons WHERE seasonKey = '$seas'";
    // $sql = "SELECT zonedesc FROM zone
    //   WHERE st_contains(geom, ST_GeomFromText('POINT($long $lat)', 4326))";

     /* Final SQL statement

      $sql = "SELECT p.name, p.scientific, p.planting, p.growing, p.pests, p.harvesting
      FROM conditions as c, plant AS p, zone AS z, seasons AS s
      WHERE c.plantKey = p.plantKey
        AND c.zoneID = z.zoneID
        AND c.seasonKey = s.seasonKey
        AND s.seasonKey = season
        AND ST_Contains(zone.polygon, ST_MakePoint(logitude, latitude));";
*/

        $sql = "SELECT p.plantKey, p.name, p.scientific, p.planting, p.growing, p.pests, p.harvesting, p.info
        FROM conditions as c, plant AS p, zone AS z, seasons AS s
        WHERE c.plantKey = p.plantKey
          AND c.zoneID = z.zoneID
          AND c.seasonKey = s.seasonKey
          AND s.seasonKey = $seas
          AND st_contains(geom, ST_GeomFromText('POINT($long $lat)', 4326))";


    //Return successful query
    $result = pg_query($sql)
      or die('Could not connect: ' . pg_last_error());

    if (!$result) {
      echo "An error occurred.\n";
      exit;
    }
    // $arr = pg_fetch_all($result);
    // print_r($arr);

    while ($row = pg_fetch_array($result)) {
      $ID = $row['plantKey'];
      $name = $row['name'];
      $scien = $row['scientific'];
      $planting = $row['planting'];
      $growing = $row['growing'];
      $pests = $row['pests'];
      $harvest = $row['harvesting'];
      $info = $row['info'];


      // Create the search results
      echo "<a href='#modal$name' class='result-link' data-toggle='modal'>";
      echo "<div class='results_item'>";
      echo "<img class='plant_img' src='assets/images/plants/$name.png'>";
      echo "<div class='plant_name'>$name</div></div></a>";

      //Create the modal
      echo "<div class='result-modal modal fade' id='modal$name' role='dialog' aria-hidden='true'>";
      echo "<div class='modal-content'><div class='close-modal' data-dismiss='modal'>";
      echo "<div class='lr'><div class='rl'></div></div></div>";
      echo "<div class='container'><div class='modal-body'>";
      echo "<img src='assets/images/plants/$name.png'><h2>$name</h2><hr>";
      echo "<h4><i>$scien</i></h4><p>$info</p><p>$planting</p><p>$growing</p>";
      echo "<p>$pests</p><p>$harvest</p>";
      echo "</div></div></div></div>";



    }


    // while ($row = pg_fetch_row($result)) {
    //   while ($data = pg_fetch_array)
    //   echo $row;
    //   echo "<br />\n";
    // }

    pg_free_result($result);
    pg_close($db);
  ?>
</body>


</html>
